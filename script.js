window.addEventListener("load", (event) => {
    let form = document.querySelector("#form")
    form.addEventListener("submit", (e) => {
        e.preventDefault()
        e.stopPropagation()
        mageHandler.init()
    })
});

const mageHandler = {
    data: null,
    datas: [],
    manifest: {
        "@context": "",
        "@id": "",
        "@type": "sc:Manifest",
        "viewingHint": "",
        "viewingDirection": "",
        "label": "",
        "metadata": [],
        "license": "",
        "sequences": []
    },
    serverUrl: null,
    resourceId: null,
    label: null,
    dataURI: null,
    toc: null,
    init: async function () {

        let file = document.querySelector("#file").files[0]
        if (file) {
            this.getToc(file).then((a) => {
                this.toc = a
            })
        }

        document.querySelector("#modal").style.display = "block"
        await new Promise(r => setTimeout(r, 50))

        let btn = document.querySelector("#submit")
        btn.setAttribute("disabled", true)
        this.serverUrl = document.querySelector("#server").value
        this.resourceId = document.querySelector("#resource").value
        this.label = document.querySelector("#label").value
        this.prefix = document.querySelector("#prefix").value
        this.suffix = document.querySelector("#suffix").value
        this.dataURI = this.serverUrl + "datas/" + this.resourceId
        this.manifest["@context"] = "http://iiif.io/api/presentation/" + document.querySelector("#manifest-version").value + "/context.json"
        this.manifest["@id"] = document.querySelector("#identifier").value
        this.manifest["label"] = (this.label) ? this.label : this.resourceId

        this.getData(this.dataURI).then(async (data) => {
            this.data = data
            this.getDatas().then((datas) => {
                this.datas = datas
                btn.removeAttribute("disabled")
                document.querySelector("#modal").style.display = "none"
                this.updateStatus(0, 0)
                console.log(this.toc)
                window.open("data:application/json," + encodeURIComponent(JSON.stringify(this.manifest)), "_blank");
            });
        })
    },

    getToc: function parse(file) {
        // Always return a Promise
        return new Promise((resolve, reject) => {
            let content = '';
            const reader = new FileReader();
            // Wait till complete
            reader.onloadend = function (e) {
                content = e.target.result;
                const result = content;
                resolve(result);
            };
            // Make sure to handle error states
            reader.onerror = function (e) {
                reject(e);
            };
            reader.readAsText(file);
        });
    },

    getDatas: async function () {
        let datas = []
        let length = this.data.files.length
        let cpt = 0
        for (const file of this.data.files) {
            cpt++
            let sha1 = file.sha1
            let url = this.serverUrl + "iiif/" + this.resourceId + "/" + sha1 + "/info.json"
            await this.getData(url).then(datafile => {
                datas.push(datafile)
            })
            this.updateStatus(cpt, length)
        }

        return datas
    },

    updateStatus: function (cpt, length) {
        let cptWrapper = document.querySelector("#cpt")
        let lengthWrapper = document.querySelector("#length")
        let progressBar = document.querySelector("#progress-bar")
        let percent = cpt / length * 100

        cptWrapper.innerText = cpt
        lengthWrapper.innerText = length
        progressBar.style.width = percent + "%"
    },

    getData: async function (url) {
        const options = {
            method: 'GET',
            mode: "cors",
            headers: { "Content-Type": "application/json" },
        };

        return await fetch(url, options)
            .then((response) => {
                if (response.ok) {
                    return response.json()
                }
            })
    }
}

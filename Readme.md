# iiif-mage-for-nakala.js
Version Javascript de [iiif mage for Nakala](https://gitlab.com/litt-arts-num/resources-iiif/iiif-mage-for-nakala)

Formulaire de création de manifest IIIF à partir d'une donnée Nakala.

## Auteurs
* Arnaud Bey
* Théo Roulet
  
## Licence
GNU GENERAL PUBLIC LICENSE Version 3
